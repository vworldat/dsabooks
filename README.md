# Universal-/DSA5-PDF-Konverter

Dieses Symfony-Programm unterstützt die Organisation deiner gekauften DSA5-PDFs. Folgende
Umwandlungen werden durchgeführt:

- Erstellung einer doppelseitigen Variante zur einfachen Betrachtung am PC-Bildschirm.
  Dabei werden ausgehend vom Inhaltsverzeichnis jeweils eine linke und rechte Seite des Buchs
  gemeinsam auf A4-Querformat zusammengefasst. Das PDF-Inhaltsverzeichnis wird automatisch
  korrigiert, sodass alle Verweise nach wie vor funktionieren
- Erstellung einer zusätzlichen Graustufen-Variante aller Dokumente. Persönliche Erfahrung hat
  gezeigt, dass diese auf schwächeren Endgeräten (z.B. Tablets) deutlich schneller laden und
  besser navigiert werden können. Dabei wird auch die Auflösung aller enthaltenen Bilder auf
  144dpi reduziert.
- Im Zuge der Konvertierung wird außerdem:
  - Der PDF-Schreibschutz/Verschlüsselung deaktiviert (notwendig, um die Änderungen durchzuführen)
  - (optional) Wasserzeichen und Personalisierung entfernt

Im Zielverzeichnis werden pro Dokument bis zu 4 Versionen erzeugt:

- `Dateiname.pdf` (bereinigt)
- `Dateiname [bw].pdf` (Graustufen)
- `Dateiname [double].pdf` (doppelseitig)
- `Dateiname [bw double].pdf` (Graustufen doppelseitig)

## Automatische Inhaltsverzeichnisse

Du hast keine Lust, zu warten, bis dein frisch erworbenes Lieblings-DSA-PDF ein korrektes
Inhaltsverzeichnis hat? Dir wird geholfen! Dieses Tool kann anhand der im Dokument benutzten
Schriften den Inhalt analysieren und automatisch ein PDF-Verzeichnis erzeugen!


## Was wird benötigt?

- PHP 7.1+ Kommandozeile (kein Webserver)
- [Robo][1]
- [Yarn][2]
- [composer][3]
- Folgende CLI-Tools in ihrer *ix-Variante:
  - `qpdf`
  - `pdftk`
  - `pdfjam`
  - `gs` (ghostscript)
  - `sed`
  - `pdftohtml`

Bisher wurde die Applikation ausschließlich unter Linux entwickelt und getestet. Falls du
sie unter Windows, z.B. mit Cygwin o.ä. zum Laufen bekommst oder kleinere Änderungen notwendig
sind, freue ich mich über deine Beteiligung!


## Installation

- Klone dir dieses Repository oder hol dir eine Kopie und entpacke sie.
- Kopiere die Datei `.env.dist` nach `.env` und passe insbesondere die Pfade nach Belieben an.
- Führe im entpackten Verzeichnis `php path/to/robo.phar init` aus. Einfacher geht es, wenn du `robo`
  irgendwo im `PATH` hinterlegst, dann reicht `robo init`

Achtung: aufgrund der vielen Zwischenschritte, und weil die Applikation nicht besonders auf
Speichernutzung optimiert wurde, sollte der temporäre Pfad mindestens 10x soviel Platz bieten
wie die Originaldateien benötigen. Der Zielpfad benötigt zusätzlich ca. 3x den Original-Platz.


## Nutzung

Über `php bin/console` kannst du die verfügbaren Symfony-Kommandos auflisten. Wenn du einfach nur
konvertieren möchtest, bietet sich entweder `php bin/console books:convert` oder die shortcut-Datei
`php bin/convert` an. Dabei gibt es folgende Optionen:

- `--remove-watermarks`: Wasserzeichen entfernen. Hierzu muss `UNMARK_PATTERN` in `.env` konfiguriert werden
- `--processes=[anzahl]`: Anzahl paralleler Threads. Die komplette Konvertierung eines einzelnen PDFs
  benötigt auf einem halbwegs aktuellen PC mehrere Minuten und belegt dabei nur einen einzelnen
  CPU-Thread. Über Parallelisierung kannst du mehrere Bücher gleichzeitig konvertieren lassen.

Sollte der Prozess abgebrochen werden, kann es passieren, dass halb geschriebene PDFs im
temp-Verzeichnis liegen bleiben, was später zu Fehlern führt. In diesem Fall sollten entweder
die betroffenen Dateien oder das gesamte Verzeichnis entfernt werden.

Beispielwerte für meinen Core i7-5820K @ 3.3GHz mit ausreichend RAM:

- Quellverzeichnis: 16 Files, 475MB
- Temp-Verzeichnis: 224 Files, 4.5GB
- Zielverzeichnis: 64 Files, 1.1GB
- Gesamtdauer der Konvertierung bei 12 parallelen Threads und voller CPU-Auslastung: ~20min


## Inhaltsverzeichnisse erzeugen

Die Erzeugung der Inhaltsverzeichnisse ist vom restlichen Konvertierungsprozess weitgehend
getrennt. Getriggert wird sie über den Befehl `php bin/console books:fill-toc` oder den
Shortcut `php bin/fill-toc`. Für eine detaillierte Ausgabe kannst du die Parameter `-vv`
oder `-vvv` anhängen.

### Ablauf

PDFs werden im Verzeichnis `NO_TOC_BOOKS_PATH` (siehe `.env`) gefunden und zuerst in
`TEMP_BOOKS_PATH/decrypted/` entsperrt und dort die vorhandenen Metadaten ausgelesen.
Per `pdftohtml` wird ein XML-Dump des PDFs in `TEMP_BOOKS_PATH/xml` erzeugt und
anschließend nach passenden Textdefinitionen durchforstet. All das, inklusive einiger
Konfigurationskonstanten, findest du in den PHP-Klassen rund um `DarkEye\XmlContent\XmlBook`.

Nach dem Parsen werden die neuen Metadaten unter
`TEMP_BOOKS_PATH/decrypted/{dateiname.pdf}.pdftk-dump-data-new` abgelegt. Daraus wird das
endgültige PDF erzeugt - zuerst in `TEMP_BOOKS_PATH/books-toc` und anschließend im
Ausgabeverzeichnis (`TOC_BOOKS_PATH`). Falls du im Inhaltsverzeichnis Anpassungen vornehmen möchtest,
kannst du die genannte `{dateiname.pdf}.pdftk-dump-data-new`-Datei in einem Texteditor
bearbeiten, die beiden nachfolgenden PDFs manuell löschen und den Vorgang erneut ausführen.
Dabei wird der von dir korrigierte Inhalt in die endgültigen PDFs geschrieben.

Der Vorgang funktioniert generell nur wenn die Überschriften (deutlich) andere Schriftgrößen
nutzen als die eigentlichen Inhalte. Bei den aktuellen DSA5-Vorlagen ist das weitgehend der
Fall (ich habe damit u.a. meine fehlerhaften PDFs aus den WdV- und Havena-Crowdfundings ergänzt).
Pro Buch waren dabei jeweils 2-5min an manuellen Korrekturen nötig, was aber in keiner
Relation zu dem Aufwand steht, ein Inhaltsverzeichnis komplett manuell zu erzeugen!


## Mitmachen

Wie schon weiter oben geschrieben, freue ich mich über jede Form der Mithilfe! Falls du Vorschläge
hast, wie das Projekt erweitert werden kann, oder Bugs findest, öffne bitte ein Ticket!


## Disclaimer

Diese Software wurde im Rahmen der [MIT-Lizenz][4] erstellt. Ich übernehme keine Verantwortung
für Schäden, die an deinem PC oder deiner PDF-Sammlung entstehen. Sorge bitte dafür, dass du
immer Backups deiner Originaldateien hast!

Das Programm richtet sich im speziellen an PDFs, welche über [ulisses-ebooks.de][5] bezogen
wurden, kann aber für beliebige Dokumente benutzt bzw. angepasst werden. Die Entfernung der
Wasserzeichen hat für mich rein ästhetische Gründe. Ich erwerbe sämtliche PDFs selbst und 
möchte keine unkontrollierte Weitergabe unterstützen. Bitte respektiere die Arbeit all jener,
die ihre Zeit und ihr Herzblut in die Erstellung von großartigen Produkten stecken!

"Das Schwarze Auge" ist eine eingetragene Marke von "Significant GbR".


[1]: https://robo.li/
[2]: https://yarnpkg.com/
[3]: https://getcomposer.org/
[4]: https://opensource.org/licenses/MIT
[5]: https://www.ulisses-ebooks.de/
