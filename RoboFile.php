<?php

/*
 * =================================================================
 * Start CI auto fetch (downloading robo dependencies automatically)
 * =================================================================.
 */
(is_dir('.ci') || mkdir('.ci')) && chdir('.ci');
if (!is_file('composer.json')) {
    exec('composer req c33s-toolkit/robo-file -n', $output, $resultCode);
    if ($resultCode > 0) {
        copy('https://getcomposer.org/composer.phar', 'composer');
        exec('php composer req c33s-toolkit/robo-file -n');
        unlink('composer');
    }
} else {
    exec('composer install --dry-run -n 2>&1', $output);
    if (false === in_array('Nothing to install or update', $output)) {
        fwrite(STDERR, "\n##### Updating .ci dependencies #####\n\n") && exec('composer install -n');
    }
}
chdir('..');
require '.ci/vendor/autoload.php';
/*
 * =================================================================
 *                        End CI auto fetch
 * =================================================================.
 */

use Consolidation\AnnotatedCommand\CommandData;

/**
 * This is project's console commands configuration for Robo task runner.
 *
 * @see http://robo.li/
 */
class RoboFile extends \Robo\Tasks implements \C33s\Robo\DefaultCi
{
    use \C33s\Robo\C33sTasks;
    // use \C33s\Robo\DebugHooksTrait;

    protected $portsToCheck = [
        // 'http' => null,
        // 'https' => null,
        // 'mysql' => null,
        // 'postgres' => null,
        // 'elasticsearch' => null,
        // 'mongodb' => null,

        // 'custom' => 'host:port',
        // 'custom' => 1234, // host defaults to localhost
    ];

    /**
     * @hook pre-command
     *
     * @see https://ci-provider.vworld.at/
     *
     * @param CommandData $commandData
     */
    public function preCommand(CommandData $commandData)
    {
        $this->assignEnvironmentConfig($commandData);
        $this->stopOnFail(true);
        $this->_prepareCiModules([
            'codeception' => '2.3.8',
            'composer' => '@latest',
            'php-cs-fixer' => 'v2.9.3',
        ]);
    }

    /**
     * Initialize project.
     */
    public function init()
    {
        if (!$this->confirmIfInteractive('Have you read the project README.md?')) {
            $this->abort();
        }

        if (!$this->ciCheckPorts($this->portsToCheck)) {
            if (!$this->confirmIfInteractive('Do you want to continue?')) {
                $this->abort();
            }
        }

//        $this->composerGlobalRequire('fxp/composer-asset-plugin', '~1.3');
        $this->composerGlobalRequire('hirak/prestissimo', '^0.3');

        $this->update();
        $this->reset();

        // ## fetch and/or resolve Symfony parameters
        // $value = $this->_resolveSymfonyParameter('foo_%param%');
        // $parameters = $this->loadSymfonyParameters($path = 'app/config/parameters.yml');

        // ## execPhp supports omitting the "php" pre-command and has an xDebug switch.
        // ## use PHP_XDEBUG_EXTENSION_PATH to configure path to xDebug .so/.dll
        // $this->_execPhp('php bin/console', $enableXDebug = false);
        // $this->_execPhp('bin/console', $enableXDebug = false);

        // ## Generic file downloads. Supported methods are auto, wget, curl and fopen
        // $this->_downloadFileToLocalPath($remoteUri, $localPath, $method = 'auto');

        // ## Print export command helper
        // $this->getExportCommand('VARIABLE_NAME', $value);

        // ## Quick port checks using fsockopen. Also see $this->ciCheckPorts($ports).
        // $this->isPortUsed($port, $host = '127.0.0.1');
    }

    /**
     * Perform code-style checks.
     *
     * @param string $arguments Optional path or other arguments
     */
    public function check($arguments = '')
    {
        $this->_execPhp("php .ci/bin/php-cs-fixer.phar fix --verbose --dry-run $arguments");
    }

    /**
     * Perform code-style checks and cleanup source code automatically.
     *
     * @param string $arguments Optional path or other arguments
     */
    public function fix($arguments = '')
    {
        if ($this->confirmIfInteractive('Do you really want to run php-cs-fixer on your source code?')) {
            $this->_execPhp("php .ci/bin/php-cs-fixer.phar fix --verbose $arguments");
        } else {
            $this->abort();
        }
    }

    /**
     * Update the Project.
     */
    public function update()
    {
        if ($this->isEnvironmentCi()) {
            $this->_execPhp('php ./.ci/bin/composer.phar install --no-progress --no-suggest --prefer-dist --optimize-autoloader');
        } else {
            $this->_execPhp('php ./.ci/bin/composer.phar install');
        }
        // $this->assetsBuild();
    }

    /**
     * Re-crates a fresh database (be careful all tables will be deleted).
     *
     * @param array $opts
     */
    public function reset($opts = ['limit' => 'none'])
    {
        if ($this->isEnvironmentProduction()) {
            if (!$this->confirmIfInteractive('THIS WILL DESTROY ALL EXISTING DATA IN YOUR PROJECT\'S DATABASE! CONTINUE?')) {
                $this->abort();
            }
        }
        // $this->_execPhp('php bin/console doctrine:schema:drop --full-database --force');
//        $this->_execPhp('php bin/console doctrine:schema:update --no-interaction --force');
//         $this->_execPhp('php bin/console doctrine:schema:create --no-interaction');
//         $this->_execPhp('php bin/console doctrine:fixtures:load --no-interaction');
//        if ($this->isEnvironmentCi()) {
//            $this->_execPhp('php bin/console doctrine:database:create --connection=legacy');
//        }
//        if ($this->isEnvironmentDevelopment()) {
//            $limit = '';
//            if ('none' != $opts['limit']) {
//                $limit = " --limit={$opts['limit']}";
//            }
//            $this->_execPhp("php bin/console app:import${limit}");
//        }

//        $adminusername = $this->getenv('demo_admin_username');
//        $adminemail = $this->getenv('demo_admin_email');
//        $adminpassword = $this->getenv('demo_admin_password');
//        $this->_execphp("bin/console fos:user:create --super-admin {$adminusername} {$adminemail} {$adminpassword}");
    }

    // not required because composer already calls yarn install. see composer.yml
//    /**
//     * Installs node packages and compile assets.
//     */
//    public function assetsInstall(): void
//    {
//        $this->_exec('yarn install');
//        $this->assetsBuild();
//    }

    /**
     * Build/Compile Encore assets.
     */
    public function assetsBuild(): void
    {
        $environment = 'dev';
        if ($this->isEnvironmentProduction()) {
            $environment = 'production';
        }
        $this->_exec("yarn run encore $environment");
    }

    /**
     * Start Encore Assets Server (not for production server!).
     */
    public function assetsServer(): void
    {
        if ($this->isEnvironmentProduction()) {
            if (!$this->confirm('YOU MUST NOT USE THIS ON THE PRODUCTION SERVER! CONTINUE?')) {
                $this->abort();
            }
        }
        $this->_exec('yarn dev-server');
    }

    /**
     * Run tests.
     */
    public function test($suite = '')
    {
        if ($this->isEnvironmentCi()) {
            $this->_execPhp("php ./vendor/codeception/codeception/codecept run $suite --coverage-xml --coverage-html --coverage-text", true);
            $this->outputCoverage();
        } else {
            $this->_execPhp("php ./vendor/codeception/codeception/codecept run $suite");
        }
    }

    /**
     * Run Unit tests.
     */
    public function testUnit()
    {
        $this->test('unit');
    }

    /**
     * Run Functional tests.
     */
    public function testFunctional()
    {
        $this->test('functional');
    }

    /**
     * Write plain coverage line used for gitlab CI detecting the coverage score.
     */
    private function outputCoverage(): void
    {
        $this->writeln(file(__DIR__.'/tests/_output/coverage.txt')[8]);
    }

    /**
     * Check if the current Environment is CI.
     *
     * @return bool
     */
    protected function isEnvironmentCi(): bool
    {
        if ('true' == getenv('CI') || '1' == getenv('CI')) {
            return true;
        }
        if ($this->hasEnv('CI_BUILD_TOKEN')) {
            return true;
        }
        if ($this->hasEnv('CI_JOB_ID')) {
            return true;
        }

        return false;
    }

    /**
     * Check if the current Environment is production.
     *
     * @return bool
     */
    protected function isEnvironmentProduction(): bool
    {
        if ('dev' == strtolower(getenv('ENVIRONMENT'))) {
            return false;
        }
        if ('development' == strtolower(getenv('ENVIRONMENT'))) {
            return true;
        }
        if ($this->isEnvironmentCi()) {
            return false;
        }

        return true;
    }

    /**
     * Check if the current Environment is Development.
     *
     * @return bool
     */
    protected function isEnvironmentDevelopment(): bool
    {
        return !$this->isEnvironmentProduction();
    }

    // =================================================================================================================
    // add you app specific code below to easily update the default robo part
    // =================================================================================================================
}
