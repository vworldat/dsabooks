const $ = require('jquery');
// JS is equivalent to the normal "bootstrap" package
// no need to set this to a variable, just require it
require('bootstrap-sass');
//<!-- IE10 viewport hack for Surface/desktop Windows 8 bug -->
require('@bower_components/ie10-viewport-bug-workaround/dist/ie10-viewport-bug-workaround.js');

const initSelect2 = require('./modules/init-select2');
const initPopover = require('./modules/init-popover');

// or you can include specific pieces
// require('bootstrap-sass/javascripts/bootstrap/tooltip');
// require('bootstrap-sass/javascripts/bootstrap/popover');

$(document).ready(function() {
    initSelect2();
    // initPopover();
});

// <link rel="stylesheet" href="select2.css">
// <link rel="stylesheet" href="select2-bootstrap.css">
// $( "#dropdown" ).select2({
//     theme: "bootstrap"
// });
//$.fn.select2.defaults.set( "theme", "bootstrap" );
