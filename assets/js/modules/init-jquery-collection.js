const $ = require('jquery');

module.exports = function() {
    $('[data-toggle="popover"]').popover();
};
