const $ = require('jquery');

module.exports = function() {
    $.fn.select2.defaults.set('theme', 'bootstrap');
    // $('.js-example-basic-multiple').select2();
    $('select').select2();
};
