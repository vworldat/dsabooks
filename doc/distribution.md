# Symfony Cat Edition

yarn encore dev
yarn add sass-loader node-sass --dev
yarn add less-loader less --dev
## Contribute

If you contribute to symfony cat edition you must git exclude the `.ci` folder. so you won't commit any files from there.


## Requirements
- yarn
- node
- php7.1


## good helpers
- nvm (node version manager) https://github.com/coreybutler/nvm-windows


## Quickstart

- edit project name in 
    - `composer.yml`
    - `package.json.dist`
- move or copy `package.json.dist` -> `package.json`
- remove `package.json.dist` from `.gitignore` if moved 
- convert `composer.yml` to `composer.json`

- edit `README.md`
- change 
    - `web/favicon.ico`
    - `web/apple-touch-icon.png`
    
## Assets / Webpack / Node / Yarn

```
yarn dev-server
yarn dev

yarn encore dev
yarn add sass-loader node-sass --dev
yarn add less-loader less --dev 
```

## Codeception

no need of running `codecept bootstrap` all files are already in the distribution

### Important things

- to add a new group create the class in `tests/_support/Group` and enable it in `codeception.yml` in the `extensions` section

### Extras

#### `CodeceptionSymfonyTrait` 
which provides shortcuts for the symfony module

#### Example Stuff

#### `setHost` for functional tests
if you use routes which only listen to a specific domain, you can easily set it with this function.

#### Namespaces
all test run under the `Tests\...` Namespace which is set in `_bootstrap.php`


### Command Examples

run all tests except the `example` group
```
codecept run -x example
```

run only the tests from the `example` group
```
codecept run -g example
```
