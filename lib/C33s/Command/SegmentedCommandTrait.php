<?php

namespace C33s\Command;

use Symfony\Component\Console\Helper\ProgressBar;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Input\InputOption;
use Symfony\Component\Console\Output\OutputInterface;
use Symfony\Component\DependencyInjection\ContainerInterface;
use Symfony\Component\Process\PhpExecutableFinder;
use Symfony\Component\Process\Process;

/**
 * This trait easily allows commands to run segmented imports or similar stuff.
 */
trait SegmentedCommandTrait
{
    /**
     * @var ProgressBar
     */
    private $mainProgress;

    /**
     * @var OutputInterface
     */
    private $mainOutput;

    /**
     * @var Process[]
     */
    private $processQueue = [];

    /**
     * @var Process[]
     */
    private $runningProcesses = [];

    /**
     * @var int
     */
    private $numberOfProcesses = 1;

    /**
     * Get number of entries to process at once.
     *
     * @return int
     */
    abstract protected function getSegmentBlockSize(): int;

    /**
     * Get count of all entries that will be processed.
     *
     * @param InputInterface $input
     *
     * @return int
     */
    abstract protected function getEntriesCount(InputInterface $input): int;

    /**
     * Execute actual stuff.
     *
     * @param InputInterface  $input
     * @param OutputInterface $output
     *
     * @return int
     */
    abstract protected function runSingleCommand(InputInterface $input, OutputInterface $output): int;

    /**
     * Provided by Symfony Command class.
     *
     * @return string The command name
     */
    abstract public function getName();

    /**
     * Provided by Symfony Command class.
     *
     * @return ContainerInterface
     *
     * @throws \LogicException
     */
    abstract protected function getContainer();

    /**
     * Call this in your configure() method.
     */
    private function configureSegmentation()
    {
        $this
            ->addOption(
                'offset',
                'o',
                InputOption::VALUE_OPTIONAL,
                'Segmentation offset',
                0
            )
            ->addOption(
                'limit',
                'l',
                InputOption::VALUE_OPTIONAL,
                'Segmentation limit',
                null
            )
            ->addOption(
                'processes',
                'p',
                InputOption::VALUE_OPTIONAL,
                'Parallel processes to use',
                1
            )
            ->addOption(
                'embedded',
                null,
                InputOption::VALUE_NONE,
                'Used by auto-segmented mode'
            )
        ;
    }

    /**
     * Call this in your execute() method.
     *
     * @param InputInterface  $input
     * @param OutputInterface $output
     */
    private function executeSingleOrSegmented(InputInterface $input, OutputInterface $output)
    {
        if (null !== $input->getOption('limit')) {
            $this->runSingleCommand($input, $output);
        } else {
            $this->runSegmentedCommands($input, $output);
        }
    }

    /**
     * Run commands in segmented sub processes.
     *
     * @param InputInterface  $input
     * @param OutputInterface $output
     */
    private function runSegmentedCommands(InputInterface $input, OutputInterface $output)
    {
        $this->numberOfProcesses = $input->getOption('processes');
        $totalCount = $this->getEntriesCount($input);
        $php = $this->fetchPhpExecutable();
        $dir = dirname($this->getContainer()->getParameter('kernel.root_dir'));

        $this->mainOutput = $output;

        $limit = $this->getSegmentBlockSize();
        $rounds = ceil($totalCount * 1.0 / $limit);

        $output->writeln("Processing {$totalCount} elements in steps of {$limit}, {$rounds} rounds in {$this->numberOfProcesses} parallel processes\n");

        $this->mainProgress = new ProgressBar($output, $totalCount * $this->getSingleProgressSteps());
        $this->mainProgress->setFormat('very_verbose');
        $this->mainProgress->start();

        $commandName = $this->getProcessCommandName($input);

        for ($i = 0; $i < $rounds; ++$i) {
            $offset = $i * $limit;
            $command = sprintf('%s bin/console %s --offset %d --limit %d --embedded --env=%s --verbose',
                $php,
                $commandName,
                $offset,
                $limit,
                $input->getOption('env')
            );

            $this->queueCommand($command, $dir);
        }

        $this->watchProcesses();

        $this->mainProgress->finish();
        $output->writeln('');
    }

    /**
     * Define the number of progress bar steps a single command should cover. This can be used to display
     * long-running single tasks in a more detailed manner.
     *
     * @return int
     */
    protected function getSingleProgressSteps(): int
    {
        return 1;
    }

    /**
     * Name of the command to execute for segmented processes. May provide additional command parameters as required.
     *
     * @param InputInterface $input
     *
     * @return string
     */
    private function getProcessCommandName(InputInterface $input): string
    {
        return $this->getName();
    }

    /**
     * Put the given command into the queue. Override to add additional options or arguments to command string.
     *
     * @param string $command
     * @param string $dir
     * @param array  $env
     */
    private function queueCommand($command, $dir)
    {
        $this->processQueue[$command] = new Process($command, $dir, null, null, null);
    }

    /**
     * Run until all processes have been finished.
     */
    private function watchProcesses()
    {
        while (count($this->processQueue) > 0 || count($this->runningProcesses) > 0) {
            if (count($this->runningProcesses) < $this->numberOfProcesses && count($this->processQueue) > 0) {
                $this->startNewProcess();
            }

            foreach ($this->runningProcesses as $command => $process) {
                if (!$process->isRunning()) {
                    // $this->getContainer()->get('logger')->debug('Command finished: '.$command);
                    unset($this->runningProcesses[$command]);
                }
            }

            usleep(50000);
        }
    }

    /**
     * Start a single command process.
     */
    private function startNewProcess()
    {
        /* @var $process Process */
        $process = current($this->processQueue);
        $command = $process->getCommandline();

        $this->runningProcesses[$command] = $process;
        unset($this->processQueue[$command]);

        // $this->getContainer()->get('logger')->debug('Command started '.$command);
        $process->start([$this, 'processCallback']);
    }

    /**
     * Get single char used to advance progress.
     *
     * @return string
     */
    protected function getAdvancementChar()
    {
        return chr(254);
    }

    /**
     * Callback used by the processes to handle progress bar advancement.
     *
     * @param string $type
     * @param string $buffer
     */
    public function processCallback($type, $buffer)
    {
        $chars = substr_count($buffer, $this->getAdvancementChar());
        if ($chars !== strlen($buffer)) {
            $this->mainOutput->writeln("\n<comment>==== Process output: ====</comment>\n  ".$buffer);
        }

        $this->mainProgress->advance($chars);
    }

    /**
     * Get path to most suitable PHP executable.
     *
     * @throws \RuntimeException
     *
     * @return string
     */
    private function fetchPhpExecutable()
    {
        $executableFinder = new PhpExecutableFinder();
        $php = $executableFinder->find();
        if (false === $php) {
            throw new \RuntimeException('Cannot find php executable');
        }

        return $php;
    }
}
