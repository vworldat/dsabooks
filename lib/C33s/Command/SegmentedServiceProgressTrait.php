<?php

namespace C33s\Command;

use Symfony\Component\Console\Helper\ProgressBar;
use Symfony\Component\Console\Output\OutputInterface;

trait SegmentedServiceProgressTrait
{
    /**
     * @var OutputInterface
     */
    protected $segmentedOutput;

    /**
     * @var ProgressBar
     */
    protected $segmentedProgress;

    /**
     * @var bool
     */
    protected $isEmbedded;

    /**
     * Start progress indication.
     *
     * @param int $steps
     */
    protected function startProgress($steps)
    {
        if (!$this->hasProgress()) {
            return;
        }

        $this->segmentedProgress = new ProgressBar($this->segmentedOutput, $steps);
    }

    /**
     * Advance progress indication.
     *
     * @param int $steps
     */
    protected function advanceProgress($steps = 1)
    {
        if ($this->isEmbedded) {
            $this->segmentedOutput->write(str_repeat(chr(254), $steps));

            return;
        }
        if (!$this->hasProgress()) {
            return;
        }

        $this->segmentedProgress->advance($steps);
    }

    /**
     * Finish progress indication.
     */
    protected function finishProgress()
    {
        if (!$this->hasProgress()) {
            return;
        }

        $this->segmentedProgress->finish();
        $this->segmentedOutput->writeln('');
    }

    /**
     * @return bool
     */
    private function hasProgress()
    {
        return
            !$this->isEmbedded &&
            $this->segmentedOutput->getVerbosity() > OutputInterface::VERBOSITY_QUIET &&
            $this->segmentedOutput->getVerbosity() < OutputInterface::VERBOSITY_VERY_VERBOSE
        ;
    }
}
