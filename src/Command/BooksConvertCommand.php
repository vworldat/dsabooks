<?php

namespace App\Command;

use App\DarkEye\BookConverter;
use C33s\Command\SegmentedCommandTrait;
use C33s\Command\SegmentedServiceProgressTrait;
use Symfony\Bundle\FrameworkBundle\Command\ContainerAwareCommand;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Input\InputOption;
use Symfony\Component\Console\Output\OutputInterface;

class BooksConvertCommand extends ContainerAwareCommand
{
    use SegmentedCommandTrait;
    use SegmentedServiceProgressTrait;

    protected function configure()
    {
        $this
            ->setName('books:convert')
            ->setDescription('Convert PDF books')
            ->addOption(
                'remove-watermarks',
                null,
                InputOption::VALUE_NONE,
                'Remove watermarks from PDF files. Requires UNMARK_PATTERN to be configured in .env.'
            )
        ;

        $this->configureSegmentation();
    }

    protected function execute(InputInterface $input, OutputInterface $output)
    {
        if (!$input->getOption('embedded')) {
            $entriesCount = $this->getEntriesCount($input);
        }

        $this->executeSingleOrSegmented($input, $output);

        if (!$input->getOption('embedded')) {
            $output->writeln('');
            $output->writeln(sprintf(
                'Processed %d books.',
                $entriesCount
            ));
        }
    }

    /**
     * Name of the command to execute for segmented processes. May provide additional command parameters as required.
     *
     * @param InputInterface $input
     *
     * @return string
     */
    private function getProcessCommandName(InputInterface $input): string
    {
        $name = $this->getName();
        if ($input->getOption('remove-watermarks')) {
            $name .= ' --remove-watermarks';
        }

        return $name;
    }

    /**
     * Get number of entries to process at once.
     *
     * @return int
     */
    protected function getSegmentBlockSize(): int
    {
        return 1;
    }

    /**
     * Define the number of progress bar steps a single command should cover. This can be used to display
     * long-running single tasks in a more detailed manner.
     *
     * @return int
     */
    protected function getSingleProgressSteps(): int
    {
        return 100;
    }

    /**
     * Get count of all entries that will be processed.
     *
     * @param InputInterface $input
     *
     * @return int
     */
    protected function getEntriesCount(InputInterface $input): int
    {
        return count($this->getBookConverter()->findBooks());
    }

    /**
     * Execute actual stuff.
     *
     * @param InputInterface  $input
     * @param OutputInterface $output
     */
    protected function runSingleCommand(InputInterface $input, OutputInterface $output): void
    {
        $this->segmentedOutput = $output;
        $this->isEmbedded = $input->getOption('embedded');

        $books = $this->getBookConverter()->findBooks();
        $books = array_slice($books, $input->getOption('offset'), $input->getOption('limit'));

        $this->startProgress(count($books) * $this->getSingleProgressSteps());

        foreach ($books as $book) {
            $advance = function ($steps = 1) {
                $this->advanceProgress($steps);
            };

            $this->getBookConverter()->convertBook($book, $advance, $input->getOption('remove-watermarks'));
        }

        $this->finishProgress();
    }

    /**
     * @return BookConverter
     */
    protected function getBookConverter()
    {
        return $this->getContainer()->get('App\DarkEye\BookConverter');
    }
}
