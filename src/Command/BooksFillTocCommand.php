<?php

namespace App\Command;

use App\DarkEye\TocParser;
use Symfony\Bundle\FrameworkBundle\Command\ContainerAwareCommand;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;

class BooksFillTocCommand extends ContainerAwareCommand
{
    protected function configure()
    {
        $this
            ->setName('books:fill-toc')
            ->setDescription('Detect and fill table of contents')
        ;
    }

    protected function execute(InputInterface $input, OutputInterface $output)
    {
        $this->getTocParser()->parseAll($output);
    }

    /**
     * @return TocParser
     */
    protected function getTocParser()
    {
        return $this->getContainer()->get('App\DarkEye\TocParser');
    }
}
