<?php

namespace App\DarkEye;

use App\DarkEye\Book\Bookmark;
use App\DarkEye\Book\MetaInfo;
use Symfony\Component\Finder\SplFileInfo;

class Book
{
    /**
     * @var SplFileInfo
     */
    private $fileInfo;

    /**
     * @var PdfHandler
     */
    private $pdfHandler;

    /**
     * @var MetaInfo
     */
    private $metaInfo;

    public function __construct(SplFileInfo $fileInfo, PdfHandler $pdfHandler)
    {
        $this->fileInfo = $fileInfo;
        $this->pdfHandler = $pdfHandler;
    }

    /**
     * @return SplFileInfo
     */
    public function getFileInfo(): SplFileInfo
    {
        return $this->fileInfo;
    }

    /**
     * @return string
     */
    public function getFilename()
    {
        return $this->fileInfo->getFilename();
    }

    /**
     * @return int
     */
    public function getNumberOfPages(): int
    {
        $this->fetchMetaInfo();

        return $this->metaInfo->getNumberOfPages();
    }

    /**
     * @return bool
     */
    public function isSinglePage(): bool
    {
        return 1 == $this->getNumberOfPages();
    }

    /**
     * @return bool
     */
    public function shouldDoublePageBeCreated(): bool
    {
        return $this->getNumberOfPages() > 5;
    }

    /**
     * @return Bookmark[]
     */
    public function getBookmarks(): array
    {
        $this->fetchMetaInfo();

        return $this->metaInfo->getBookmarks();
    }

    /**
     * @return bool
     */
    public function hasBookmarks(): bool
    {
        return count($this->getBookmarks()) > 0;
    }

    /**
     * @return MetaInfo
     */
    public function getMetaInfo(): MetaInfo
    {
        $this->fetchMetaInfo();

        return $this->metaInfo;
    }

    private function fetchMetaInfo()
    {
        if (null === $this->metaInfo) {
            $this->metaInfo = $this->pdfHandler->fetchMetaInfoForBook($this);
        }
    }
}
