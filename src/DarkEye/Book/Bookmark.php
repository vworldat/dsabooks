<?php

namespace App\DarkEye\Book;

class Bookmark implements DumpableMetrics
{
    /**
     * @var string
     */
    private $title;

    /**
     * @var int
     */
    private $level;

    /**
     * @var int
     */
    private $pageNumber;

    /**
     * Bookmark constructor.
     *
     * @param string $title
     * @param int    $level
     * @param int    $pageNumber
     */
    public function __construct(string $title, $level, $pageNumber)
    {
        $this->title = $title;
        $this->level = (int) $level;
        $this->pageNumber = (int) $pageNumber;
    }

    /**
     * @return string
     */
    public function getTitle(): string
    {
        return $this->title;
    }

    /**
     * @param string $title
     */
    public function setTitle(string $title): void
    {
        $this->title = $title;
    }

    /**
     * @return int
     */
    public function getLevel(): int
    {
        return $this->level;
    }

    /**
     * @param int $level
     */
    public function setLevel(int $level): void
    {
        $this->level = $level;
    }

    /**
     * @return int
     */
    public function getPageNumber(): int
    {
        return $this->pageNumber;
    }

    /**
     * @param int $pageNumber
     */
    public function setPageNumber(int $pageNumber): void
    {
        $this->pageNumber = $pageNumber;
    }

    public function toRawMetrics(): string
    {
        return <<<EOF
BookmarkBegin
BookmarkTitle: {$this->title}
BookmarkLevel: {$this->level}
BookmarkPageNumber: {$this->pageNumber}
EOF;
    }
}
