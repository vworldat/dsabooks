<?php

namespace App\DarkEye\Book;

interface DumpableMetrics
{
    /**
     * Convert this object to its raw string representation as dumped/read by pdftk dump_data.
     *
     * @return string
     */
    public function toRawMetrics(): string;
}
