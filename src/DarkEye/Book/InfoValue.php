<?php

namespace App\DarkEye\Book;

class InfoValue implements DumpableMetrics
{
    /**
     * @var string
     */
    private $key;

    /**
     * @var string
     */
    private $value;

    public function __construct(string $key, string $value)
    {
        $this->key = $key;
        $this->value = $value;
    }

    /**
     * @return string
     */
    public function getKey(): string
    {
        return $this->key;
    }

    /**
     * @return string
     */
    public function getValue(): string
    {
        return $this->value;
    }

    /**
     * @param string $value
     */
    public function setValue(string $value): void
    {
        $this->value = $value;
    }

    /**
     * Convert this object to its raw string representation as dumped/read by pdftk dump_data.
     *
     * @return string
     */
    public function toRawMetrics(): string
    {
        $value = str_replace("\n", "\r", trim($this->value));

        return <<<EOF
InfoBegin
InfoKey: {$this->key}
InfoValue: {$value}
EOF;
    }
}
