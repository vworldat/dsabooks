<?php

namespace App\DarkEye\Book;

use App\DarkEye\XmlContent\Outline;

class MetaInfo implements DumpableMetrics
{
    /**
     * @var array
     */
    private $metrics;

    public function __construct(array $metrics)
    {
        $this->metrics = $this->enhanceMetrics($metrics);
    }

    private function enhanceMetrics(array $rawMetrics)
    {
        if (!isset($rawMetrics['Bookmark']) || !is_array($rawMetrics['Bookmark'])) {
            $rawMetrics['Bookmark'] = [];
        }

        $rawMetrics['Bookmark'] = array_map(function ($data) {
            return new Bookmark($data['BookmarkTitle'], $data['BookmarkLevel'], $data['BookmarkPageNumber']);
        }, $rawMetrics['Bookmark']);
        $rawMetrics['Info'] = array_map(function ($data) {
            return new InfoValue($data['InfoKey'], $data['InfoValue']);
        }, $rawMetrics['Info']);

        if (!isset($rawMetrics['PageLabel']) || !is_array($rawMetrics['PageLabel'])) {
            $rawMetrics['PageLabel'] = [];
        }

        return $rawMetrics;
    }

    /**
     * @return int
     */
    public function getNumberOfPages(): int
    {
        return (int) $this->metrics['NumberOfPages'];
    }

    /**
     * @return Bookmark[]
     */
    public function getBookmarks(): array
    {
        return $this->metrics['Bookmark'];
    }

    /**
     * @return array
     */
    public function getMetrics(): array
    {
        return $this->metrics;
    }

    /**
     * Remove personal information and unique ids from metadata.
     */
    public function clearPersonalInformation(): void
    {
        unset($this->metrics['PdfID0']);
        unset($this->metrics['PdfID1']);

        foreach ($this->metrics['Info'] as $value) {
            /* @var $value InfoValue */
            switch ($value->getKey()) {
                case 'CreationDate':
                case 'ModDate':
                    $value->setValue("D:20150101120000+01'00'");
                    break;

                case 'PXCViewerInfo':
                    $value->setValue('');
            }
        }
    }

    /**
     * Convert this object to its raw string representation as dumped/read by pdftk dump_data.
     *
     * @return string
     */
    public function toRawMetrics(): string
    {
        $parts = [];
        foreach ($this->metrics as $key => $metric) {
            if (is_array($metric)) {
                foreach ($metric as $item) {
                    if ($item instanceof DumpableMetrics) {
                        $parts[] = $item->toRawMetrics();
                    } else {
                        $parts[] = $this->dumpArrayItem($key, $item);
                    }
                }
            } else {
                $parts[] = "{$key}: {$metric}";
            }
        }

        return implode("\n", $parts);
    }

    private function dumpArrayItem(string $prefix, array $item)
    {
        $dumped = "{$prefix}Begin";
        foreach ($item as $key => $value) {
            $dumped .= "\n{$key}: {$value}";
        }

        return $dumped;
    }

    /**
     * Convert bookmark page numbers to double-paged layout, removing table of content offset.
     */
    public function convertToDoublePages()
    {
        $toc = $this->detectTableOfContentPageNumber();
        $numberingOffset = $this->detectPageNumbersOffset();
        $removePages = $toc - 1;

        foreach ($this->getBookmarks() as $bookmark) {
            $pdfPageNumber = $bookmark->getPageNumber();

            // append original actual page number to bookmark title
            $actualPageNumber = max(1, $pdfPageNumber - $numberingOffset);
            $bookmark->setTitle($bookmark->getTitle()." [{$actualPageNumber}]");

            $newPageNumber = max(1, ceil(($pdfPageNumber - $removePages) / 2));
            $bookmark->setPageNumber($newPageNumber);
        }

        $newLabels = [];
        foreach ($this->metrics['PageLabel'] as $label) {
            if ($label['PageLabelNewIndex'] < $toc) {
                // drop everything that will be cut
                continue;
            }
            $label['PageLabelNewIndex'] = ceil(($label['PageLabelNewIndex'] - $removePages) / 2);

            $newLabels[] = $label;
        }
        $this->metrics['PageLabel'] = $newLabels;
    }

    /**
     * Detect the PDF page number where the table of content can be found. If there is none, the first page is returned.
     *
     * @return int
     */
    public function detectTableOfContentPageNumber()
    {
        foreach ($this->getBookmarks() as $bookmark) {
            if ($bookmark->getPageNumber() > 10) {
                continue;
            }

            $title = mb_strtolower($bookmark->getTitle());
            if ('inhaltsverzeichnis' === $title || 'inhalt' === $title) {
                return $bookmark->getPageNumber();
            }
        }

        return 1;
    }

    /**
     * Detect the offset between visible page numbers and actual PDF pages. This can be found in the 'PageLabel'
     * section of the metrics definition.
     *
     * e.g.:
     * - PageLabelNewIndex: 1, PageLabelStart: 1
     * - PageLabelNewIndex: 3, PageLabelStart: 1 => re-numbering starts at page 3, providing a total offset of 2
     *
     * @return int
     */
    private function detectPageNumbersOffset()
    {
        if (count($this->metrics['PageLabel']) < 2) {
            return 0;
        }
        if ($this->metrics['PageLabel'][1]['PageLabelNewIndex'] > $this->detectTableOfContentPageNumber()) {
            return 0;
        }

        $pageDiff = $this->metrics['PageLabel'][1]['PageLabelNewIndex'] - $this->metrics['PageLabel'][0]['PageLabelNewIndex'];
        $numberDiff = $this->metrics['PageLabel'][1]['PageLabelStart'] - $this->metrics['PageLabel'][0]['PageLabelStart'];

        return $pageDiff - $numberDiff;
    }

    private function getTitle()
    {
        foreach ($this->metrics['Info'] as $metric) {
            /* @var $metric InfoValue */
            if ('Title' === $metric->getKey()) {
                return $metric->getValue();
            }
        }

        return 'unknown';
    }

    public function createBookmarksFromOutline(Outline $outline, $title = null)
    {
        if (null === $title) {
            $title = $this->getTitle();
        }

        $this->metrics['Bookmark'] = [];
        $this->metrics['Bookmark'][] = new Bookmark($title, 1, 1);

        foreach ($outline->getItems() as $item) {
            $this->metrics['Bookmark'][] = new Bookmark(
                htmlspecialchars_decode($item->getTitle(), ENT_QUOTES),
                $item->getLevel() + 1,
                $item->getPageNumber()
            );
        }
    }
}
