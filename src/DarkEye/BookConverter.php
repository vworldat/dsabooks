<?php

namespace App\DarkEye;

use Psr\Log\LoggerInterface;
use Symfony\Component\Console\Output\OutputInterface;
use Symfony\Component\Filesystem\Filesystem;
use Symfony\Component\Finder\Finder;
use Symfony\Component\Finder\SplFileInfo;

class BookConverter
{
    /**
     * @var string
     */
    private $booksDir;

    /**
     * @var string
     */
    private $outputDir;

    /**
     * @var string
     */
    private $tempDir;

    /**
     * @var LoggerInterface
     */
    private $logger;

    /**
     * @var PdfHandler
     */
    private $pdfHandler;

    /**
     * @var Book[]
     */
    private $books;

    public function __construct(
        string $booksDir,
        string $outputDir,
        string $tempDir,
        LoggerInterface $logger,
        PdfHandler $pdfHandler
    ) {
        $this->booksDir = $booksDir;
        $this->outputDir = $outputDir;
        $this->tempDir = $tempDir;
        $this->logger = $logger;
        $this->pdfHandler = $pdfHandler;
    }

    /**
     * @param OutputInterface $output
     * @param bool            $removeWatermarks
     */
    public function convertAll(OutputInterface $output, bool $removeWatermarks)
    {
        $books = $this->findBooks();
        $output->writeln('Found '.count($books).' books');

        foreach ($books as $book) {
            $this->convertBook($book, function () {}, $removeWatermarks);
        }
    }

    /**
     * @param Book     $book
     * @param callable $advance
     * @param bool     $removeWatermarks
     */
    public function convertBook(Book $book, callable $advance, $removeWatermarks = false): void
    {
        $this->logger->info('Processing '.$book->getFileInfo()->getRelativePathname());
        $fileInfo = $this->pdfHandler->getDecryptedFileForBook($book);

        if ($this->finishedFileExists($fileInfo, ' [bw]')) {
            $this->logger->info('Skipping '.$book->getFileInfo()->getRelativePathname().' because file exists');
            $advance(100);

            return;
        }

        $advance(10);
        if ($removeWatermarks) {
            $fileInfo = $this->pdfHandler->removeWatermarks($book);
            $advance(10);
            $book->getMetaInfo()->clearPersonalInformation();
            $advance(10);
        }

        $fileInfo = $this->pdfHandler->addMetricsToPdf($fileInfo, $book->getMetaInfo());
        $this->copyToFinishedFolder($fileInfo);
        $advance(10);

        $pages = $book->getNumberOfPages();
        $progress = 0;
        $maxProgress = 25;
        $callback = function ($type, $buffer) use ($pages, &$progress, $maxProgress, &$advance) {
            if (preg_match('/^Page (\d+)$/', $buffer, $matches)) {
                while ($matches[1] / $pages > $progress / $maxProgress) {
                    ++$progress;
                    $advance();
                }
            }
        };
        $grayFile = $this->pdfHandler->convertPdfToGrayscale($fileInfo, '06-grayscale', $callback);
        $this->copyToFinishedFolder($grayFile, ' [bw]');

        if ($book->shouldDoublePageBeCreated()) {
            $doubleFileInfo = $this->pdfHandler->convertPdfToDoublePaged($fileInfo, $book->getMetaInfo());
            $this->copyToFinishedFolder($doubleFileInfo, ' [double]');
            $advance(10);

            // this is a rough guess, but there is no way to determine the actual amount without re-scanning the PDF
            $pages = $book->getNumberOfPages() / 2;
            $progress = 0;
            $maxProgress = 25;
            $callback = function ($type, $buffer) use ($pages, &$progress, $maxProgress, &$advance) {
                if (preg_match('/^Page (\d+)$/', $buffer, $matches)) {
                    while ($matches[1] / $pages > $progress / $maxProgress) {
                        ++$progress;
                        $advance();
                    }
                }
            };
            $grayFile = $this->pdfHandler->convertPdfToGrayscale($doubleFileInfo, '10-grayscale-double', $callback);
            $this->copyToFinishedFolder($grayFile, ' [bw double]');
        } else {
            $advance(35);
        }
    }

    /**
     * @return Book[]
     */
    public function findBooks(): array
    {
        if (null === $this->books) {
            $this->logger->info('Searching for books in '.$this->booksDir);
            $finder = Finder::create()
                ->name('*.pdf')
                ->sortByName()
                ->in($this->booksDir)
            ;

            $this->books = [];
            foreach ($finder as $file) {
                /* @var $file SplFileInfo */
                $this->logger->info('  Found book '.$file->getRelativePathname());
                $this->books[] = new Book($file, $this->pdfHandler);
            }
        }

        return $this->books;
    }

    /**
     * @param SplFileInfo $fileInfo
     * @param string      $addSuffix
     */
    private function copyToFinishedFolder(SplFileInfo $fileInfo, $addSuffix = ''): void
    {
        $pathname = $fileInfo->getRelativePathname();
        $pathname = '/'.implode('/', array_slice(explode('/', ltrim($pathname, '/')), 1));
        if (!empty($addSuffix)) {
            $pathname = dirname($pathname).'/'.basename($pathname, '.pdf').$addSuffix.'.pdf';
        }

        $fs = new Filesystem();
        $fs->copy($fileInfo->getPathname(), $this->outputDir.$pathname);
    }

    /**
     * @param SplFileInfo $fileInfo
     * @param string      $addSuffix
     *
     * @return bool
     */
    private function finishedFileExists(SplFileInfo $fileInfo, $addSuffix = ''): bool
    {
        $pathname = $fileInfo->getRelativePathname();
        $pathname = '/'.implode('/', array_slice(explode('/', ltrim($pathname, '/')), 1));
        if (!empty($addSuffix)) {
            $pathname = dirname($pathname).'/'.basename($pathname, '.pdf').$addSuffix.'.pdf';
        }

        return (new Filesystem())->exists($this->outputDir.$pathname);
    }
}
