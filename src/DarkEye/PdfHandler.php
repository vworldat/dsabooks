<?php

namespace App\DarkEye;

use App\DarkEye\Book\MetaInfo;
use Cocur\Slugify\SlugifyInterface;
use Psr\Log\LoggerInterface;
use Symfony\Component\Filesystem\Filesystem;
use Symfony\Component\Finder\SplFileInfo;
use Symfony\Component\Process\ExecutableFinder;
use Symfony\Component\Process\Process;

class PdfHandler
{
    const REQUIRED_EXECUTABLES = ['qpdf', 'pdftk', 'pdfjam', 'pdftohtml', 'gs', 'sed'];
    const DIFFERENCES_PATTERN = 's:^/Differences.*:/Differences \[1\]:';

    /**
     * @var SlugifyInterface
     */
    private $slugify;

    /**
     * @var string
     */
    private $tempDir;

    /**
     * @var LoggerInterface
     */
    private $logger;

    /**
     * @var string
     */
    private $unMarkPattern;

    /**
     * @var array
     */
    private $executables = [];

    public function __construct(
        SlugifyInterface $slugify,
        string $tempDir,
        LoggerInterface $logger,
        string $unMarkPattern
    ) {
        $this->slugify = $slugify;
        $this->tempDir = $tempDir;
        $this->logger = $logger;
        $this->unMarkPattern = $unMarkPattern;

        $this->findExecutables();
    }

    /**
     * Process the given PDF and extract available meta info.
     *
     * @param Book $book
     *
     * @return MetaInfo
     */
    public function fetchMetaInfoForBook(Book $book): MetaInfo
    {
        $decryptedFile = $this->getDecryptedFileForBook($book);

        $rawMetrics = $this->extractPageMetrics($decryptedFile);

        return new MetaInfo($rawMetrics);
    }

    /**
     * Decrypt the given PDF.
     *
     * @param Book $book
     *
     * @return SplFileInfo
     */
    public function getDecryptedFileForBook(Book $book): SplFileInfo
    {
        return $this->decryptFile($book->getFileInfo());
    }

    /**
     * Get XML representation of content.
     *
     * @param Book $book
     *
     * @return SplFileInfo
     */
    public function getXmlFileForBook(Book $book): SplFileInfo
    {
        return $this->convertPdfToXml($book->getFileInfo());
    }

    /**
     * Remove watermark from PDF.
     *
     * @param Book $book
     *
     * @return SplFileInfo
     */
    public function removeWatermarks(Book $book): SplFileInfo
    {
        $decrypted = $this->getDecryptedFileForBook($book);
        $unMarked = $this->unMarkFile($decrypted);

        return $unMarked;
    }

    public function addMetricsToPdf(SplFileInfo $fileInfo, MetaInfo $metaInfo, $directory = '05-meta-enhanced')
    {
        $this->logger->debug('Adding new metrics to file '.$fileInfo->getRelativePathname());
        $metrics = $metaInfo->toRawMetrics();
        $metricsPath = $fileInfo->getPathname().'.pdftk-dump-data-new';
        if (file_exists($metricsPath)) {
            $this->logger->info('New metrics file '.$fileInfo->getRelativePathname().'.pdftk-dump-data-new already exists');
        } else {
            file_put_contents($metricsPath, $metrics);
        }

        return $this->processFileStep(
            $fileInfo,
            $directory,
            'pdftk',
            "%1\$s '%2\$s' update_info_utf8 '$metricsPath' output '%3\$s'"
        );
    }

    public function convertPdfToDoublePaged(SplFileInfo $fileInfo, MetaInfo $metaInfo): SplFileInfo
    {
        $this->logger->info('Converting PDF to double pages '.$fileInfo->getRelativePathname());
        $startPage = $metaInfo->detectTableOfContentPageNumber();
        if ($startPage > 1) {
            $endPage = $metaInfo->getNumberOfPages();

            $fileInfo = $this->processFileStep(
                $fileInfo,
                '07-removed-before-toc',
                'pdftk',
                "%1\$s '%2\$s' cat {$startPage}-{$endPage} output '%3\$s'"
            );
        }

        $fileInfo = $this->processFileStep(
            $fileInfo,
            '08-doubled',
            'pdfjam',
            '%1$s --nup 2x1 --landscape \'%2$s\' --outfile \'%3$s\''
        );

        $metaInfo->convertToDoublePages();

        return $this->addMetricsToPdf($fileInfo, $metaInfo, '09-doubled-meta-enhanced');
    }

    private function findExecutables()
    {
        $this->logger->debug('Initializing executables');
        $finder = new ExecutableFinder();
        foreach (self::REQUIRED_EXECUTABLES as $bin) {
            $executable = $finder->find($bin);
            if (null === $executable) {
                throw new \RuntimeException('Cannot find "'.$bin.'" executable');
            }

            $this->logger->debug('  Found '.$bin.' at '.$executable);
            $this->executables[$bin] = $executable;
        }
    }

    /**
     * @param SplFileInfo $fileInfo
     *
     * @return SplFileInfo The decrypted temporary file
     */
    private function decryptFile(SplFileInfo $fileInfo): SplFileInfo
    {
        return $this->processFileStep(
            $fileInfo,
            '01-decrypted',
            'qpdf',
            '%1$s --decrypt \'%2$s\' \'%3$s\'',
            false
        );
    }

    private function convertPdfToXml(SplFileInfo $fileInfo)
    {
        $pathname = $fileInfo->getRelativePathname();

        $targetDir = 'xml';
        $targetFile = str_replace('//', '/', "/{$targetDir}/{$pathname}.xml");
        $targetPath = $this->tempDir.$targetFile;
        if (file_exists($targetPath)) {
            $this->logger->info(ucfirst($targetDir).' file "'.$targetFile.'" already exists');

            return new SplFileInfo($targetPath, dirname($targetFile), $targetFile);
        }

        $fs = new Filesystem();
        $fs->mkdir(dirname($targetPath));

        $this->logger->info('Processing file to "'.$targetFile.'"');
        $commandSyntax = '%1$s -noframes -xml -i -hidden -fontfullname -enc UTF-8 \'%2$s\' \'%3$s\'';
        $command = sprintf(
            $commandSyntax,
            $this->executables['pdftohtml'],
            $fileInfo->getPathname(),
            substr($targetPath, 0, -4)
        );
        $this->runCommand($command);

        if (!file_exists($targetPath)) {
            throw new \RuntimeException("There was an error processing the file. Command:\n$command");
        }

        return new SplFileInfo($targetPath, dirname($targetFile), $targetFile);
    }

    /**
     * @param SplFileInfo   $fileInfo
     * @param string        $directory
     * @param callable|null $callback
     *
     * @return SplFileInfo The decrypted temporary file
     */
    public function convertPdfToGrayscale(SplFileInfo $fileInfo, $directory = '06-grayscale', callable $callback = null): SplFileInfo
    {
        $command = <<<'COMMAND'
%1$s
    -sOutputFile='%3$s' 
    -sDEVICE=pdfwrite
    -dPDFSETTINGS=/screen
    -sColorConversionStrategy=Gray
    -dProcessColorModel=/DeviceGray
    -dCompatibilityLevel=1.4
    -dDownsampleColorImages=true
    -dDownsampleGrayImages=true
    -dDownsampleMonoImages=true
    -dColorImageResolution=144
    -dGrayImageResolution=144
    -dMonoImageResolution=144
    -dDetectDuplicateImages=true
    -dNumRenderingThreads=6
    -dMaxBitmap=0
    -c "120000000 setvmthreshold"
    -dNOPAUSE
    -dBATCH
    '%2$s'
COMMAND;

        return $this->processFileStep(
            $fileInfo,
            $directory,
            'gs',
            str_replace("\n", ' ', $command),
            true,
            $callback
        );
    }

    /**
     * @param SplFileInfo $fileInfo
     *
     * @return SplFileInfo The unmarked temporary file
     */
    private function unMarkFile(SplFileInfo $fileInfo): SplFileInfo
    {
        if (empty($this->unMarkPattern)) {
            $this->logger->warning('Cannot remove watermark when there is no unmark pattern defined. Set UNMARK_PATTERN in your .env file.');

            return $fileInfo;
        }

        $uncompressed = $this->processFileStep(
            $fileInfo,
            '02-uncompressed',
            'pdftk',
            '%1$s \'%2$s\' output \'%3$s\' uncompress'
        );

        $pattern = str_replace('\\', '\\\\', $this->unMarkPattern);
        $differencesPattern = str_replace('\\', '\\\\', self::DIFFERENCES_PATTERN);
        $unmarkedUncompressed = $this->processFileStep(
            $uncompressed,
            '03-unmarked-uncompressed',
            'sed',
            '%1$s -e \''.$pattern.'\' -e \''.$differencesPattern.'\' < \'%2$s\' > \'%3$s\''
        );

        return $this->processFileStep(
            $unmarkedUncompressed,
            '04-unmarked-compressed',
            'pdftk',
            '%1$s \'%2$s\' output \'%3$s\' compress'
        );
    }

    /**
     * @param SplFileInfo   $fileInfo
     * @param string        $targetDir
     * @param string        $executable
     * @param string        $commandSyntax
     * @param bool          $removeLeadingDir
     * @param callable|null $callback
     *
     * @return SplFileInfo The uncompressed temporary file
     */
    private function processFileStep(
        SplFileInfo $fileInfo,
        $targetDir,
        $executable,
        $commandSyntax,
        $removeLeadingDir = true,
        callable $callback = null
    ): SplFileInfo {
        $pathname = $fileInfo->getRelativePathname();
        if ($removeLeadingDir) {
            $pathname = '/'.implode('/', array_slice(explode('/', ltrim($pathname, '/')), 1));
        }

        $targetFile = str_replace('//', '/', "/{$targetDir}/{$pathname}");
        $targetPath = $this->tempDir.$targetFile;
        if (file_exists($targetPath)) {
            $this->logger->info(ucfirst($targetDir).' file "'.$targetFile.'" already exists');

            return new SplFileInfo($targetPath, dirname($targetFile), $targetFile);
        }

        $fs = new Filesystem();
        $fs->mkdir(dirname($targetPath));

        $this->logger->info('Processing file to "'.$targetFile.'"');
        $command = sprintf($commandSyntax, $this->executables[$executable], $fileInfo->getPathname(), $targetPath);
        $this->runCommand($command, $callback);

        if (!file_exists($targetPath)) {
            throw new \RuntimeException("There was an error processing the file. Command:\n$command");
        }

        return new SplFileInfo($targetPath, dirname($targetFile), $targetFile);
    }

    /**
     * @param SplFileInfo $decryptedFile
     *
     * @return array
     */
    private function extractPageMetrics(SplFileInfo $decryptedFile)
    {
        $metaFile = $decryptedFile->getPathname().'.pdftk-dump-data';
        if (file_exists($metaFile)) {
            $rawData = file_get_contents($metaFile);
        } else {
            $this->logger->info('Extracting metrics from file "'.$decryptedFile->getRelativePathname().'"');
            $command = sprintf("%s '%s' dump_data_utf8", $this->executables['pdftk'], $decryptedFile->getPathname());
            $rawData = $this->runCommand($command);
            file_put_contents($metaFile, $rawData);
        }
        $rawData = str_replace("\r\n", "\n", $rawData);
        $rawData = str_replace("\r", ' ', $rawData);

        $lines = explode("\n", $rawData);
        $metrics = [];
        $currentSection = '';
        $currentValues = [];
        $lastValue = '';

        foreach ($lines as $line) {
            $line = trim($line);
            if (preg_match('/^([\w]+)Begin$/', $line, $matches)) {
                if (!empty($currentValues)) {
                    $metrics[$currentSection][] = $currentValues;
                }
                $currentSection = $matches[1];
                $currentValues = [];

                continue;
            }

            if (!$this->lineHasValidMetaPrefix($line)) {
                $lastValue .= "\n".$line;

                continue;
            }

            list($key, $value) = explode(':', $line, 2);
            $value = trim($value);
            $value = $this->fixValue($value, $key);

            if (0 === strpos($key, $currentSection)) {
                $currentValues[$key] = $value;
                $lastValue = &$currentValues[$key];
            } else {
                if (!empty($currentValues)) {
                    $metrics[$currentSection][] = $currentValues;
                    $currentValues = [];
                }
                $metrics[$key] = $value;
                $lastValue = &$metrics[$key];
            }
        }
        if (!empty($currentValues)) {
            $metrics[$currentSection][] = $currentValues;
        }

        return $metrics;
    }

    /**
     * @param string        $command
     * @param callable|null $callback
     *
     * @return string
     */
    private function runCommand(string $command, callable $callback = null): string
    {
        $this->logger->debug('Executing '.$command);

        $process = new Process($command);
        $process
            ->setTimeout(null)
            ->run($callback)
        ;

        return $process->getOutput();
    }

    /**
     * Fix common issues with metrics values.
     *
     * @param string $value
     * @param string $key
     *
     * @return string
     */
    private function fixValue(string $value, string $key): string
    {
        $value = str_replace('Inhaltsverzeichsnis', 'Inhaltsverzeichnis', $value);

        return $value;
    }

    /**
     * Check if the given metadata line contains a valid prefix.
     *
     * @param string $line
     *
     * @return bool
     */
    private function lineHasValidMetaPrefix(string $line): bool
    {
        $validPrefixes = [
            'InfoBegin',
            'InfoKey:',
            'InfoValue:',
            'PdfID0:',
            'PdfID1:',
            'NumberOfPages:',
            'BookmarkBegin',
            'BookmarkTitle:',
            'BookmarkLevel:',
            'BookmarkPageNumber:',
            'PageMediaBegin',
            'PageMediaNumber:',
            'PageMediaRotation:',
            'PageMediaRect:',
            'PageMediaDimensions:',
            'PageLabelBegin',
            'PageLabelNewIndex:',
            'PageLabelStart:',
            'PageLabelPrefix:',
            'PageLabelNumStyle:',
        ];

        foreach ($validPrefixes as $validPrefix) {
            if (0 === strpos($line, $validPrefix)) {
                return true;
            }
        }

        return false;
    }
}
