<?php

namespace App\DarkEye;

use App\DarkEye\XmlContent\XmlBook;
use Psr\Log\LoggerInterface;
use Symfony\Component\Console\Output\OutputInterface;
use Symfony\Component\Filesystem\Filesystem;
use Symfony\Component\Finder\Finder;
use Symfony\Component\Finder\SplFileInfo;

class TocParser
{
    /**
     * @var string
     */
    private $booksDir;

    /**
     * @var string
     */
    private $outputDir;

    /**
     * @var string
     */
    private $tempDir;

    /**
     * @var LoggerInterface
     */
    private $logger;

    /**
     * @var PdfHandler
     */
    private $pdfHandler;

    /**
     * @var Book[]
     */
    private $books;

    public function __construct(
        string $booksDir,
        string $outputDir,
        string $tempDir,
        LoggerInterface $logger,
        PdfHandler $pdfHandler
    ) {
        $this->booksDir = $booksDir;
        $this->outputDir = $outputDir;
        $this->tempDir = $tempDir;
        $this->logger = $logger;
        $this->pdfHandler = $pdfHandler;
    }

    /**
     * @param OutputInterface $output
     */
    public function parseAll(OutputInterface $output)
    {
        $books = $this->findBooks();
        $output->writeln('Found '.count($books).' books');

        foreach ($books as $book) {
            $this->parseBook($book, $output);
        }
    }

    /**
     * @param Book            $book
     * @param OutputInterface $output
     */
    public function parseBook(Book $book, OutputInterface $output): void
    {
        $this->logger->info('Processing '.$book->getFileInfo()->getRelativePathname());
        $xmlFile = $this->pdfHandler->getXmlFileForBook($book);

        $metaInfo = $this->pdfHandler->fetchMetaInfoForBook($book);
        $xmlBook = new XmlBook($xmlFile, $book);

        $outline = $xmlBook->detectOutline();
        $metaInfo->createBookmarksFromOutline($outline, $book->getFileInfo()->getBasename('.pdf'));

        $decrypted = $this->pdfHandler->getDecryptedFileForBook($book);
        $tocFile = $this->pdfHandler->addMetricsToPdf($decrypted, $metaInfo, '50-books-toc');

        $this->copyToFinishedFolder($tocFile);
    }

    /**
     * @return Book[]
     */
    public function findBooks(): array
    {
        if (null === $this->books) {
            $this->logger->info('Searching for books in '.$this->booksDir);
            $finder = Finder::create()
                ->name('*.pdf')
                ->sortByName()
                ->in($this->booksDir)
            ;

            $this->books = [];
            foreach ($finder as $file) {
                /* @var $file SplFileInfo */
                $this->logger->info('  Found book '.$file->getRelativePathname());
                $this->books[] = new Book($file, $this->pdfHandler);
            }
        }

        return $this->books;
    }

    /**
     * @param SplFileInfo $fileInfo
     * @param string      $addSuffix
     */
    private function copyToFinishedFolder(SplFileInfo $fileInfo, $addSuffix = ''): void
    {
        $pathname = $fileInfo->getRelativePathname();
        $pathname = '/'.implode('/', array_slice(explode('/', ltrim($pathname, '/')), 1));
        if (!empty($addSuffix)) {
            $pathname = dirname($pathname).'/'.basename($pathname, '.pdf').$addSuffix.'.pdf';
        }

        $fs = new Filesystem();
        $fs->copy($fileInfo->getPathname(), $this->outputDir.$pathname);
    }

    /**
     * @param SplFileInfo $fileInfo
     * @param string      $addSuffix
     *
     * @return bool
     */
    private function finishedFileExists(SplFileInfo $fileInfo, $addSuffix = ''): bool
    {
        $pathname = $fileInfo->getRelativePathname();
        $pathname = '/'.implode('/', array_slice(explode('/', ltrim($pathname, '/')), 1));
        if (!empty($addSuffix)) {
            $pathname = dirname($pathname).'/'.basename($pathname, '.pdf').$addSuffix.'.pdf';
        }

        return (new Filesystem())->exists($this->outputDir.$pathname);
    }
}
