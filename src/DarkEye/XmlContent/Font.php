<?php

namespace App\DarkEye\XmlContent;

use SimpleXMLElement;

class Font
{
    const COLOR_FOCUS_RULE = '#cd1719';

    /**
     * @var int
     */
    private $id;

    /**
     * @var int
     */
    private $size;

    /**
     * @var string
     */
    private $family;

    /**
     * @var string
     */
    private $color;

    public function __construct(SimpleXMLElement $xmlFont)
    {
        $this->id = (int) $xmlFont['id'];
        $this->size = (int) $xmlFont['size'];
        $this->family = (string) $xmlFont['family'];
        $this->color = (string) $xmlFont['color'];
    }

    /**
     * @return int
     */
    public function getId(): int
    {
        return $this->id;
    }

    /**
     * @return int
     */
    public function getSize(): int
    {
        return $this->size;
    }

    /**
     * @return string
     */
    public function getFamily(): string
    {
        return $this->family;
    }

    /**
     * @return string
     */
    public function getColor(): string
    {
        return $this->color;
    }

    public function isFocusRule(): bool
    {
        return self::COLOR_FOCUS_RULE === $this->color;
    }

    public function isBold(): bool
    {
        return strpos($this->family, '-Bold') > 0;
    }
}
