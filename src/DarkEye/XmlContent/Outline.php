<?php

namespace App\DarkEye\XmlContent;

class Outline
{
    const SAFE_TITLES = [
        'inhaltsverzeichnis',
        'vorwort',
        'anhang',
        'index',
    ];

    const CHAPTER = 'kapitel';
    const MIN_CHAPTERS = 3;

    /**
     * @var OutlineItem[]
     */
    private $items = [];

    /**
     * @var OutlineItem
     */
    private $lastItem;

    public function addItem(OutlineItem $item)
    {
        if (null !== $this->lastItem && $this->lastItem->shouldBeMergedWith($item)) {
            $this->lastItem->mergeWith($item);
        } else {
            if (null !== $this->lastItem) {
                while ($item->getLevel() > $this->lastItem->getLevel() + 1) {
                    $item->dedent();
                }
            }
            $this->items[$item->getHash()] = $item;
            $this->lastItem = $item;
        }
    }

    /**
     * Detect if there are main chapters on the same levels as regular chapters and indent the regular ones.
     */
    public function fixMainChapters(): void
    {
        if (!$this->hasMainChapters()) {
            return;
        }

        $indent = false;
        $chapterLevel = 1;
        foreach ($this->items as $item) {
            $name = mb_strtolower($item->getTitle());
            if (in_array($name, self::SAFE_TITLES)) {
                continue;
            }
            if (0 === mb_strpos($name, self::CHAPTER)) {
                $chapterLevel = $item->getLevel();
                $indent = false;

                continue;
            }
            if ($item->getLevel() === $chapterLevel) {
                $indent = true;
            }

            if ($indent) {
                $item->indent();

                continue;
            }
        }
    }

    private function hasMainChapters()
    {
        $chapterCount = 0;
        foreach ($this->items as $item) {
            if ($item->getLevel() > 1) {
                continue;
            }
            $name = mb_strtolower($item->getTitle());
            if (0 === mb_strpos($name, self::CHAPTER)) {
                ++$chapterCount;
            }
        }

        return $chapterCount >= self::MIN_CHAPTERS;
    }

    public function __toString(): string
    {
        $output = '';
        foreach ($this->items as $item) {
            $fontIds = '';
            foreach ($item->getOutlineLevel()->getFonts() as $font) {
                $fontIds .= ','.$font->getId();
            }
            $fontIds = ltrim($fontIds, ',');
            $output .= sprintf(
                "%s[%03d] {%s} %s\n",
                str_repeat(' ', $item->getLevel() * 4),
                $item->getPageNumber(),
                $fontIds,
                $item->getTitle()
            );
        }

        return $output;
    }

    /**
     * @return OutlineItem[]
     */
    public function getItems(): array
    {
        return $this->items;
    }
}
