<?php

namespace App\DarkEye\XmlContent;

class OutlineItem
{
    /**
     * @var int
     */
    private $pageNumber;

    /**
     * @var OutlineLevel
     */
    private $outlineLevel;

    /**
     * @var int
     */
    private $level;

    /**
     * @var string
     */
    private $title;

    /**
     * @var Text[]
     */
    private $texts = [];

    public function __construct(int $pageNumber, OutlineLevel $outlineLevel, Text $firstText)
    {
        $this->pageNumber = $pageNumber;
        $this->outlineLevel = $outlineLevel;
        $this->level = $outlineLevel->getLevel();
        $this->title = $firstText->getContent();
        $this->texts[$firstText->getHash()] = $firstText;
    }

    /**
     * @return int
     */
    public function getPageNumber(): int
    {
        return $this->pageNumber;
    }

    /**
     * @return OutlineLevel
     */
    public function getOutlineLevel(): OutlineLevel
    {
        return $this->outlineLevel;
    }

    /**
     * @return string
     */
    public function getTitle(): string
    {
        return $this->title;
    }

    /**
     * @return Text[]
     */
    public function getTexts(): array
    {
        return $this->texts;
    }

    public function getHash(): string
    {
        return spl_object_hash($this);
    }

    public function shouldBeMergedWith(self $item)
    {
        if ($item->getPageNumber() !== $this->getPageNumber()) {
            return false;
        }

        foreach ($item->getTexts() as $thatText) {
            foreach ($this->getTexts() as $thisText) {
                if ($thatText->getFontId() !== $thisText->getFontId()) {
                    return false;
                }
            }
        }

        foreach ($item->getTexts() as $thatText) {
            foreach ($this->getTexts() as $thisText) {
                $topDiff = abs($thatText->getTop() - $thisText->getTop());
                $leftDiff = abs($thatText->getLeft() - $thisText->getLeft());
                if (0 === $leftDiff && $topDiff < $thisText->getHeight()) {
                    return true;
                }
            }
        }

        return false;
    }

    public function mergeWith(self $item)
    {
        if ('-' === mb_substr($this->title, -1)) {
            $firstChar = mb_substr($item->getTitle(), 0, 1);
            if ($firstChar === mb_strtoupper($firstChar)) {
                $this->title .= $item->getTitle();
            } else {
                $this->title = substr($this->title, 0, -1).$item->getTitle();
            }
        } else {
            $this->title .= ' '.$item->getTitle();
        }
    }

    /**
     * Get the actual numeric level used in the end.
     *
     * @return int
     */
    public function getLevel(): int
    {
        return $this->level;
    }

    /**
     * Indent this item 1 step.
     */
    public function indent(): void
    {
        ++$this->level;
    }

    /**
     * Dedent this item 1 step.
     */
    public function dedent(): void
    {
        --$this->level;
    }
}
