<?php

namespace App\DarkEye\XmlContent;

class OutlineLevel
{
    /**
     * @var int
     */
    private $level;

    /**
     * @var Font[]
     */
    private $fonts = [];

    /**
     * OutlineLevel constructor.
     *
     * @param int $level
     */
    public function __construct(int $level)
    {
        $this->level = $level;
    }

    /**
     * @param Font $font
     */
    public function addFont(Font $font)
    {
        $this->fonts[] = $font;
    }

    /**
     * @return int
     */
    public function getLevel(): int
    {
        return $this->level;
    }

    /**
     * @return array
     */
    public function getFonts(): array
    {
        return $this->fonts;
    }

    public function matchesText(Text $text): bool
    {
        foreach ($this->fonts as $font) {
            if ($font->getId() === $text->getFontId()) {
                return true;
            }
        }

        return false;
    }
}
