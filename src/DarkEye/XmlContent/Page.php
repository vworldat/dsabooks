<?php

namespace App\DarkEye\XmlContent;

use SimpleXMLElement;

class Page
{
    /**
     * @var int
     */
    private $top;

    /**
     * @var int
     */
    private $left;

    /**
     * @var int
     */
    private $width;

    /**
     * @var int
     */
    private $height;

    /**
     * @var int
     */
    private $number;

    /**
     * @var string
     */
    private $position;

    /**
     * @var Text[]
     */
    private $texts = [];

    public function __construct(SimpleXMLElement $xmlPage)
    {
        $this->top = (int) $xmlPage['top'];
        $this->left = (int) $xmlPage['left'];
        $this->width = (int) $xmlPage['width'];
        $this->height = (int) $xmlPage['height'];
        $this->number = (int) $xmlPage['number'];
        $this->position = (string) $xmlPage['position'];

        foreach ($xmlPage->text as $xmlText) {
            $this->texts[] = new Text($xmlText);
        }
        $this->sortTexts();
    }

    /**
     * @return int
     */
    public function getTop(): int
    {
        return $this->top;
    }

    /**
     * @return int
     */
    public function getLeft(): int
    {
        return $this->left;
    }

    /**
     * @return int
     */
    public function getWidth(): int
    {
        return $this->width;
    }

    /**
     * @return int
     */
    public function getHeight(): int
    {
        return $this->height;
    }

    /**
     * @return int
     */
    public function getNumber(): int
    {
        return $this->number;
    }

    /**
     * @return string
     */
    public function getPosition(): string
    {
        return $this->position;
    }

    /**
     * @return Text[]
     */
    public function getTexts(): array
    {
        return $this->texts;
    }

    /**
     * @param Font $font
     *
     * @return Text[]
     */
    public function getTextsUsingFont(Font $font): array
    {
        return array_filter($this->texts, function (Text $text) use ($font) {
            return $text->getFontId() === $font->getId() && $text->hasVisibleContent();
        });
    }

    public function getTextsContaining(string $contain): array
    {
        return array_filter($this->texts, function (Text $text) use ($contain) {
            return false !== strpos($text->getContent(), $contain);
        });
    }

    public function isTocPage()
    {
        return count($this->getTextsContaining('Inhaltsverzeichnis')) > 0;
    }

    private function sortTexts()
    {
        usort($this->texts, function (Text $a, Text $b) {
            if ($this->textsAreInDifferentHalfColumns($a, $b)) {
                return $a->getLeft() <=> $b->getLeft();
            }
            if ($a->getTop() === $b->getTop()) {
                return $a->getLeft() <=> $b->getLeft();
            }

            return $a->getTop() <=> $b->getTop();
        });
    }

    private function textsAreInDifferentHalfColumns(Text $a, Text $b): bool
    {
        $center = $this->width / 2;

        return
            ($a->getLeft() < $center && $b->getLeft() > $center) ||
            ($b->getLeft() < $center && $a->getLeft() > $center)
        ;
    }
}
