<?php

namespace App\DarkEye\XmlContent;

use SimpleXMLElement;

class Text
{
    /**
     * @var int
     */
    private $top;

    /**
     * @var int
     */
    private $left;

    /**
     * @var int
     */
    private $width;

    /**
     * @var int
     */
    private $height;

    /**
     * @var int
     */
    private $fontId;

    /**
     * @var string
     */
    private $content;

    public function __construct(SimpleXMLElement $xmlText)
    {
        $this->top = (int) $xmlText['top'];
        $this->left = (int) $xmlText['left'];
        $this->width = (int) $xmlText['width'];
        $this->height = (int) $xmlText['height'];
        $this->fontId = (int) $xmlText['font'];
        $this->content = (string) trim(strip_tags($xmlText->asXML()));
    }

    /**
     * @return int
     */
    public function getTop(): int
    {
        return $this->top;
    }

    /**
     * @return int
     */
    public function getLeft(): int
    {
        return $this->left;
    }

    /**
     * @return int
     */
    public function getWidth(): int
    {
        return $this->width;
    }

    /**
     * @return int
     */
    public function getHeight(): int
    {
        return $this->height;
    }

    /**
     * @return int
     */
    public function getFontId(): int
    {
        return $this->fontId;
    }

    /**
     * @return string
     */
    public function getContent(): string
    {
        return $this->content;
    }

    public function getHash(): string
    {
        return spl_object_hash($this);
    }

    /**
     * Check if this text contains anything else but empty html tags or (page) numbers or index chars or tab fillers.
     *
     * @return bool
     */
    public function hasVisibleContent()
    {
        return
            !empty($this->content) &&
            !preg_match('#^\d+$#', $this->content) &&
            mb_strlen($this->content) > 1 &&
            !preg_match('#^[A-Z] und [A-Z]$#', $this->content) &&
            false === strpos($this->content, '................')
        ;
    }
}
