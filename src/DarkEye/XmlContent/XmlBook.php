<?php

namespace App\DarkEye\XmlContent;

use App\DarkEye\Book;
use SimpleXMLElement;
use Symfony\Component\Finder\SplFileInfo;

class XmlBook
{
    const MIN_FONT_SIZE = 17;
    const MIN_NON_BOLD_FONT_SIZE = 22;
    const MAX_FONT_DISTANCE = 4;

    /**
     * @var Font[]
     */
    private $fonts = [];

    /**
     * @var Page[]
     */
    private $pages = [];

    /**
     * @var Book
     */
    private $book;

    public function __construct(SplFileInfo $xmlFile, Book $book)
    {
        $this->parseXml($xmlFile);
        $this->book = $book;
    }

    private function parseXml(SplFileInfo $xmlFile)
    {
        $content = file_get_contents($xmlFile->getPathname());
        $content = str_replace(['<i>', '</i>'], '', $content);
        $xml = new SimpleXMLElement($content);

        foreach ($xml->page as $xmlPage) {
            foreach ($xmlPage->fontspec as $xmlFont) {
                $font = new Font($xmlFont);
                $this->fonts[$font->getId()] = $font;
            }
            $page = new Page($xmlPage);
            $this->pages[$page->getNumber()] = $page;
        }
    }

    public function detectOutline()
    {
        $mainFonts = $this->detectMainFonts();
        $levels = $this->groupFontsToLevels($mainFonts);
        // dump($levels);

        $startNumber = 0;
        $tocPage = $this->detectTocPage();
        if (null !== $tocPage) {
            $startNumber = $tocPage->getNumber();
        }
        $outline = new Outline();
        $texts = [];
        foreach ($this->pages as $page) {
            if ($page->getNumber() < $startNumber) {
                continue;
            }
            foreach ($page->getTexts() as $text) {
                if (!$text->hasVisibleContent()) {
                    continue;
                }
                if (isset($texts[$page->getNumber()][$text->getContent()])) {
                    continue;
                }
                $texts[$page->getNumber()][$text->getContent()] = $text;
                foreach ($levels as $outlineLevel) {
                    if ($outlineLevel->matchesText($text)) {
                        $outlineItem = new OutlineItem($page->getNumber(), $outlineLevel, $text);
                        $outline->addItem($outlineItem);
                    }
                }
            }
        }
        $outline->fixMainChapters();

        return $outline;
    }

    private function detectMainFonts()
    {
        $this->orderFontsBySize();
        $fonts = $this->fonts;
        $fonts = $this->filterFontsBySize($fonts);
        $fonts = $this->filterFontsUsedOnLastPages($fonts);
        $fonts = $this->filterFontsNotUsedAfterToc($fonts);

        return $fonts;
    }

    private function orderFontsBySize()
    {
        uasort($this->fonts, function (Font $a, Font $b) {
            if ($b->getSize() === $a->getSize()) {
                return $a->getId() <=> $b->getId();
            }

            return $b->getSize() <=> $a->getSize();
        });
    }

    private function getPagesUsingFont(Font $font)
    {
        return $this->getPagesUsingFonts([$font]);
    }

    private function getPagesUsingFonts(array $fonts)
    {
        return array_filter($this->pages, function (Page $page) use ($fonts) {
            foreach ($fonts as $font) {
                if (count($page->getTextsUsingFont($font)) > 0) {
                    return true;
                }
            }

            return false;
        });
    }

    /**
     * @param Font[] $fonts
     *
     * @return Font[]
     */
    private function filterFontsBySize(array $fonts)
    {
        $fonts = array_filter($fonts, function (Font $font) {
            return
                $font->getSize() >= self::MIN_FONT_SIZE &&
                !$font->isFocusRule() &&
                ($font->getSize() >= self::MIN_NON_BOLD_FONT_SIZE || $font->isBold())
            ;
        });

        return $fonts;
    }

    /**
     * @param Font[] $fonts
     *
     * @return Font[]
     */
    private function filterFontsUsedOnLastPages(array $fonts): array
    {
        $pagesTotal = count($this->pages);
        $fonts = array_filter($fonts, function (Font $font) use ($pagesTotal) {
            $pages = $this->getPagesUsingFont($font);

            $minPage = $pagesTotal;
            foreach ($pages as $page) {
                $minPage = min($minPage, $page->getNumber());
            }

            return $minPage < $pagesTotal - 2;
        });

        return $fonts;
    }

    /**
     * @param Font[] $fonts
     *
     * @return Font[]
     */
    private function filterFontsNotUsedAfterToc(array $fonts): array
    {
        $tocPage = $this->detectTocPage();
        if (null === $tocPage) {
            return $fonts;
        }

        $fonts = array_filter($fonts, function (Font $font) use ($tocPage) {
            $pages = $this->getPagesUsingFont($font);

            foreach ($pages as $page) {
                if ($page->getNumber() > $tocPage->getNumber()) {
                    return true;
                }
            }

            return false;
        });

        return $fonts;
    }

    private function detectTocPage(): ? Page
    {
        foreach ($this->pages as $page) {
            if ($page->isTocPage()) {
                return $page;
            }
        }

        return null;
    }

    /**
     * Group fonts by size, creating the outline levels.
     *
     * @param Font[] $fonts
     *
     * @return OutlineLevel[]
     */
    private function groupFontsToLevels(array $fonts): array
    {
        $levels = [];
        $lastFont = null;
        $intLevel = 1;
        foreach ($fonts as $font) {
            if (null === $lastFont || ($lastFont->getSize() - $font->getSize()) > self::MAX_FONT_DISTANCE) {
                $level = new OutlineLevel($intLevel++);
                $levels[] = $level;
            } else {
                $level = $levels[count($levels) - 1];
            }

            $level->addFont($font);
            $lastFont = $font;
        }

        return $levels;
    }
}
