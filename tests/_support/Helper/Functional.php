<?php

namespace Helper;

use Tests\Traits\CodeceptionSymfonyTrait;

// here you can define custom actions
// all public methods declared in helper class will be available in $I

class Functional extends \Codeception\Module
{
    use CodeceptionSymfonyTrait;

    /**
     * Sets the Host which will be simulated to access. This is necessary to access routes which only listen to a specific
     * host.
     * If not set `localhost` will be used.
     *
     * @param $host
     */
    public function setHost($host)
    {
        $this->getClient()->setServerParameter('HTTP_HOST', $host);
    }
}
