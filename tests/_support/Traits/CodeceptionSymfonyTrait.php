<?php

namespace Tests\Traits;

trait CodeceptionSymfonyTrait
{
    /**
     * Returns the Codeception Symfony Module.
     *
     * @return \Codeception\Module\Symfony2
     */
    private function getSymfony()
    {
        return $this->getModule('Symfony2');
    }

    /**
     * Returns the Symfony Container.
     *
     * @return \Symfony\Component\DependencyInjection\Container
     */
    private function getContainer()
    {
        return $this->getSymfony()->_getContainer();
    }

    /**
     * Returns the Special Symfony2Connector (\Symfony\Component\HttpKernel\Client).
     *
     * @see \Codeception\Module\Symfony2::_before() there `$this->client` gets overwritten with  `= new Symfony2Connector($this->kernel);`
     *
     * @return \Codeception\Lib\Connector\Symfony2
     */
    private function getClient()
    {
        return $this->getSymfony()->client;
    }

    private function getEntityManager()
    {
        return $this->getContainer()->get('doctrine');
    }
}
