<?php

namespace Test\Functional;

use FunctionalTester;

/**
 * @group example
 */
class ExampleCest
{
    public function _before(FunctionalTester $I)
    {
        $I->setHost('localhost');
    }

    public function _after(FunctionalTester $I)
    {
    }

    public function ExampleUrlCest(FunctionalTester $I)
    {
        $I->amOnPage('/');
        $I->see('Hello, world');
    }
}
