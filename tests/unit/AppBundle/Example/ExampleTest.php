<?php

namespace Tests\Unit\AppBundle\Example;

/**
 * @group example
 */
class ExampleTest extends \Codeception\Test\Unit
{
    protected function _before()
    {
    }

    protected function _after()
    {
    }

    /**
     * @example
     * @dataProvider provideExampleData
     *
     * @param $exampleValue
     */
    public function testValidateForValidResults(string $exampleValue)
    {
        $this->assertTrue(in_array($exampleValue, ['1', '2', '3']), 'Invalid Result with Example Test');
    }

    public function provideExampleData()
    {
        return [
            ['1'],
            ['2'],
            ['3'],
        ];
    }
}
