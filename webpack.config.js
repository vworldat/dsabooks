var Encore = require('@symfony/webpack-encore');
//https://stackoverflow.com/questions/45698467/webpack-encore-jquery-plugins-out-of-view
Encore
    .setOutputPath('public/build/') // the project directory where compiled assets will be stored
    .setPublicPath('/build') // the public path used by the web server to access the previous directory
    .cleanupOutputBeforeBuild()
    // .enableSassLoader()
    .enableSassLoader(function(sassOptions) {}, {
        resolveUrlLoader: false
    })
    //.enableLessLoader()
    .enableSourceMaps(!Encore.isProduction())
    .enableVersioning(Encore.isProduction()) // uncomment to create hashed filenames (e.g. app.abc123.css)
    .autoProvidejQuery() // uncomment for legacy applications that require $/jQuery as a global variable
    //.autoProvideVariables({
    //     $: 'jquery',
    //     jQuery: 'jquery',
    //     'window.jQuery': 'jquery',
    // })
    .enablePostCssLoader() // http://postcss.org/
//######################################################################################################################
//    _                                _       _
//   (_) __ ___   ____ _ ___  ___ _ __(_)_ __ | |_ ___
//   | |/ _` \ \ / / _` / __|/ __| '__| | '_ \| __/ __|
//   | | (_| |\ V / (_| \__ \ (__| |  | | |_) | |_\__ \
//  _/ |\__,_| \_/ \__,_|___/\___|_|  |_| .__/ \__|___/
// |__/                                 |_|
//######################################################################################################################
    .addEntry('js/app', './assets/js/app.js')
    .addEntry('js/html5shiv', './node_modules/html5shiv/dist/html5shiv.js')
    .addEntry('js/respond', './node_modules/respond.js/src/respond.js')
    //.addEntry('js/ie10-viewport-bug-workaround', './assets/js/ie10-viewport-bug-workaround.js') //loaded in app.js
//######################################################################################################################
//     _         _           _               _
// ___| |_ _   _| | ___  ___| |__   ___  ___| |_ ___
/// __| __| | | | |/ _ \/ __| '_ \ / _ \/ _ \ __/ __|
//\__ \ |_| |_| | |  __/\__ \ | | |  __/  __/ |_\__ \
//|___/\__|\__, |_|\___||___/_| |_|\___|\___|\__|___/
//         |___/
//######################################################################################################################
    .addStyleEntry('css/app', './assets/css/app.scss')
    //.addStyleEntry('css/app', './assets/css/app.less')
    .addStyleEntry('css/ie10-viewport-bug-workaround', './node_modules/@bower_components/ie10-viewport-bug-workaround/dist/ie10-viewport-bug-workaround.css')
//######################################################################################################################
//      _                        _
//  ___| |__   __ _ _ __ ___  __| |
// / __| '_ \ / _` | '__/ _ \/ _` |
// \__ \ | | | (_| | | |  __/ (_| |
// |___/_| |_|\__,_|_|  \___|\__,_|
//
//######################################################################################################################
    .createSharedEntry('vendor', [ //https://symfony.com/doc/current/frontend/encore/shared-entry.html
        // js (vendor.js)
        'jquery',
        'bootstrap-sass',
        'select2',

        // css (vendor.css)
        //'bootstrap-sass/assets/stylesheets/_bootstrap.scss',
        // '~@bower_components/ie10-viewport-bug-workaround/dist/ie10-viewport-bug-workaround.css', // loaded separate in base.html.twig
        'bootstrap-sass/assets/stylesheets/_bootstrap.scss',
        'font-awesome/scss/font-awesome.scss',
        'select2/dist/css/select2.css',
        'select2-bootstrap-theme/dist/select2-bootstrap.css'
    ])



;
var config = Encore.getWebpackConfig();
// console.log(config);
// config.resolve.extensions.push('json');
// config.watchOptions = { poll: true, ignored: /node_modules/ };
module.exports = config;



// addPlugin(plugin, priority = 0) {
//     webpackConfig.addPlugin(plugin, priority);
//
//     return this;
// },
// addLoader(loader) {
//     webpackConfig.addLoader(loader);
//
//     return this;
// },
// enablePostCssLoader(postCssLoaderOptionsCallback = () => {}) {
//     webpackConfig.enablePostCssLoader(postCssLoaderOptionsCallback);
//
//     return this;
// },
// *     Encore.configureFilenames({
//     *         js: '[name].[chunkhash].js',
// *         css: '[name].[contenthash].css',
// *         images: 'images/[name].[hash:8].[ext]',
// *         fonts: 'fonts/[name].[hash:8].[ext]'
// *     });

//encode api
//https://github.com/symfony/webpack-encore/blob/master/index.js
// { context: 'C:\\home\\src',
//     entry:
//     { 'js/app': './assets/js/app.js',
//         'js/html5shiv': './node_modules/html5shiv/dist/html5shiv.js',
//         'js/respond': './node_modules/respond.js/src/respond.js',
//         vendor:
//         [ 'jquery',
//             'bootstrap-sass',
//             'bootstrap-sass/assets/stylesheets/_bootstrap.scss' ],
//             'css/app': './assets/css/app.scss',
//         'css/ie10-viewport-bug-workaround': './node_modules/@bower_components/bootstrap3-ie10-viewport-bug-workaro
//         und/ie10-viewport-bug-workaround.css' },
//         output:
//         { path: 'D:\\myhome\\projects\\development\\symfony-cat-edition\\src\\public\\build',
//             filename: '[name].js',
//             publicPath: 'http://localhost:8080/build/',
//             pathinfo: true },
//         module: { rules: [ [Object], [Object], [Object], [Object], [Object] ] },
//         plugins:
//             [ ExtractTextPlugin { filename: '[name].css', id: 1, options: [Object] },
//                 DeleteUnusedEntriesJSPlugin { entriesToDelete: [Array] },
//                 ManifestPlugin { opts: [Object] },
//                 LoaderOptionsPlugin { options: [Object] },
//                 NamedModulesPlugin { options: {} },
//                 ProvidePlugin { definitions: [Object] },
//                 CleanWebpackPlugin { paths: [Array], options: [Object] },
//                 CommonsChunkPlugin {
//                 chunkNames: [Array],
//                 filenameTemplate: undefined,
//                 minChunks: Infinity,
//                 selectedChunks: undefined,
//                 children: undefined,
//                 deepChildren: undefined,
//                 async: undefined,
//                 minSize: undefined,
//                 ident: 'D:\\myhome\\projects\\development\\symfony-cat-edition\\src\\node_modules\\webpack\\lib\\optimiz
//                 e\\CommonsChunkPlugin.js0' },
//         DefinePlugin { definitions: [Object] },
//         FriendlyErrorsWebpackPlugin {
//         compilationSuccessInfo: [Object],
//             onErrors: undefined,
//             shouldClearConsole: false,
//             formatters: [Array],
//             transformers: [Array] } ],
//         devtool: 'inline-source-map',
//             devServer:
//         { contentBase: 'D:\\myhome\\projects\\development\\symfony-cat-edition\\src\\public',
//             publicPath: 'http://localhost:8080/build/',
//             headers: { 'Access-Control-Allow-Origin': '*' },
//             hot: false,
//                 quiet: true,
//             compress: true,
//             historyApiFallback: true,
//             watchOptions: { ignored: /node_modules/ },
//             https: undefined },
//         performance: { hints: false },
//         stats:
//         { hash: false,
//             version: false,
//             timings: false,
//             assets: false,
//             chunks: false,
//             maxModules: 0,
//             modules: false,
//             reasons: false,
//             children: false,
//             source: false,
//             errors: false,
//             errorDetails: false,
//             warnings: false,
//             publicPath: false },
//         resolve:
//         { extensions: [ '.js', '.jsx', '.vue', '.ts', '.tsx' ],
//             alias: {} } }
